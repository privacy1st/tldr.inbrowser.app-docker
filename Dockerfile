# "ADD <git-URL>" is not yet stable Dockerfile syntax.
#ADD https://github.com/InBrowserApp/tldr.inbrowser.app.git /app
#
FROM debian:stable-slim AS git-clone
RUN apt-get update \
    && apt-get install -y git \
    && rm -rf /var/lib/apt/lists/*
RUN git clone --single-branch https://github.com/InBrowserApp/tldr.inbrowser.app.git /data
FROM node:alpine as builder
COPY --from=git-clone /data /usr/src/app

WORKDIR /usr/src/app
RUN npm install --progress
# Build to ./dist/
RUN npm run build

FROM nginx:alpine as webserver
COPY --from=builder /usr/src/app/dist /usr/share/nginx/html
