# tldr InBrowser.App Docker

Nginx (Alpine) based Docker image containing the [tldr InBrowser.App](https://github.com/InBrowserApp/tldr.inbrowser.app) web app.

[This git repository is available on Codeberg](https://codeberg.org/privacy1st/tldr.inbrowser.app-docker).

## Build

See [Dockerfile](Dockerfile).

Build script: https://codeberg.org/privacy1st/ContainerImages

## Example: Serve the app with Nginx

To build the Docker image and start it, run:

```shell
sudo docker compose up -d
```

`sudo docker stats` reports that it has a memory footprint of 19 MB (while idle).

## License

MIT, see [LICENSE](LICENSE).
